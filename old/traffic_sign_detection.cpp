/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include "traffic_sign_detection.h"
#include "lane_detection.h"

vector<Mat> api_lane_detection(Mat src) {
    Mat white, blue, red;
    Mat dst;
    white = white_detector(src);
    blue = blue_detector(src);

    dst = white.clone();
    addWeighted(dst, 1, blue, 1, 0.0, dst);
    GaussianBlur(dst, dst, Size(3, 3), 0, 0);

    red = red_detector(src);
    GaussianBlur(red, red, Size(3, 3), 0, 0);

    vector<Mat> tmp_result, result;
    result = extract_candidate_img(dst, src, false);
    tmp_result = extract_candidate_img(red, src, true);

    for (int i = 0; i < tmp_result.size(); i++) {
        result.push_back(tmp_result[i]);
    }
    return result;
}

Mat white_detector(Mat src) {
    Mat hsv_img;
    Mat dst;
    cvtColor(src, hsv_img, cv::COLOR_BGR2HSV);
    inRange(hsv_img, Scalar(95, 90, 92), Scalar(111, 192, 230), dst);
    return dst;
}

Mat white_detector(Mat src) {
    Mat hls_img;
    Mat dst;
    cvtColor(src, hls_img, cv::COLOR_BGR2HLS);
    inRange(hls_img, Scalar(90, 229, 240), Scalar(115, 260, 255), dst);
    return dst;
}

Mat red_detector(Mat src) {
    Mat hsv_img;
    Mat dst;
    cvtColor(src, hsv_img, cv::COLOR_BGR2HSV);
    inRange(hsv_img, Scalar(160, 120, 140), Scalar(185, 170, 200), dst);
    return dst;
}

vector<Mat> extract_candidate_img(Mat src, Mat raw, bool isRed) {
    vector<vector<Point> > cnts;
    vector<Vec4i> hierarchy;
    vector<Mat> result;

    findContours(src, cnts, hierarchy, RETR_EXTERNAL, 1, Point(0, 0));
    for (int i = 0; i < cnts.size(); i++) {
        vector<Point> cnt = cnts[i];
        Rect rect = boundingRect(cnt);
        int height = rect.height;
        int width = rect.width;
        if (contourArea(cnt) > 2500 && abs(width - height) < 3) {
            int x = rect.x;
            int y = rect.y;
            if (!checked(x, y, width, height, raw) && !isRed) {
                continue;
            }
            result.push_back(ROI(raw, x, y, width, height));
        }
    }
    return result;
}
