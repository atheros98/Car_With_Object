#include "frame_processing.h"
#include "lane_utils.h"
/*Main function*/
Mat frame_processing(Mat src) {
    Mat dst;

    //Start Processing
    resize(src, src, Size(WIDTH, HEIGHT));
    //First ROI to cut off Fucking objects
//    Point offset = Point(0, 80);
//    int vect[] = {offset.x, offset.y, WIDTH, HEIGHT - offset.y};
//    //Process with the whole frame
//    dst = ROI(src, vect);
    //    dst = colorFilter(dst);
    dst = src.clone();
    dst = grayScale(dst);
//    dst = smoothImage(dst);
    dst = contoursMat(dst);

    return dst;
}
Mat smoothImage(Mat src) {
    Mat dst = src.clone();
    int count_black = 0;
    int count_white = 0;
    int offset = 4;
    int threshold = (int) (offset * offset * 4) * 60 / 100;
    for (int y = offset; y < dst.rows - offset; y++) {
        for (int x = offset; x < dst.cols - offset; x++) {
            if (dst.at<uchar>(y, x) == 255) {
                count_black = 0;
                for (int a = y - offset; a < y + offset; a++) {
                    for (int b = x - offset; b < x + offset; b++) {
                        if (dst.at<uchar>(a, b) == 0) {
                            count_black++;
                        }
                    }
                }
                if (count_black > threshold)
                    dst.at<uchar>(y, x) = 0;
            } else {
                count_white = 0;
                for (int a = y - offset; a < y + offset; a++) {
                    for (int b = x - offset; b < x + offset; b++) {
                        if (dst.at<uchar>(a, b) == 255) {
                            count_white++;
                        }
                    }
                }
                if (count_white > threshold)
                    dst.at<uchar>(y, x) = 255;
            }
        }
    }
    return dst;
}
