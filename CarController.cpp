#include "cam_interact/camera_controller.h"
#include "traffic_sign/traffic_sign_recognition.h"
#include "driving_control/driving_controller.h"
#include "GPIO/gpio_controller.h"
#include "video_logger/video_logger.h"
/*nam anh api*/
//#include "api_lane_detection/cross_detection.h"
#include "api_lane_detection/frame_processing.h"
#include "api_lane_detection/lane_detection.h"
#include "traffic_sign/traffic_sign_recognition.h"
#include "api_lane_detection/lane_utils.h"

/*-----------*/
/*minh api*/
#include "minhcht_lane_detection/minhCode.h"
/*--------*/
int main(int argc, char* argv[]) {
    int init_throttle = atoi(argv[1]);

    //init component
    init_engine(init_throttle);
    init_device();
    init_color_stream();
    init_depth_stream();
    init_gpio();
    init_video_writer(Size(320,240));
    while (true) {
        double start = getTickCount();
        Mat frame = get_color_frame();
        imshow("frame", frame);
//        /*-------------------------------------------*/
        Mat processed_frame = frame_processing(frame);

        Point offset = Point(0, 60);
        int vect[] = {offset.x, offset.y, WIDTH, HEIGHT - offset.y};
        //Process with the whole frame
        Mat birdFrame = ROI(processed_frame, vect);

        birdFrame = birdEyeView(birdFrame);
        Mat emp = Mat::zeros(birdFrame.size(),CV_8UC1);
        birdFrame = drawLine(birdFrame,emp);
//        vector <Mat> tranformed = pipeLine(frame);
////        bool has_cross = cross_check_visual(processed_frame);
        Mat gray;
        double steering_angle = lane_get_contour_visual(processed_frame, gray);
        bool has_cross = crossProcessVisual(gray, birdFrame);
//        bool has_cross = false;
        if (has_cross) {
            cout<<"cross";
            cout<<"\n";
        }
        if(steering_angle<0) {
            steering_angle=steering_angle*3;
        } else {
            steering_angle=steering_angle*4.5;
        }
        /*-------------------------------------------*/
//        Mat gray;
//        vector <Mat> tranformed = pipeLine(frame);
//        double steering_angle = angleProcessVisual(gray, tranformed[1],tranformed[0]);
//        cout << steering_angle;
//        cout << "\n";
//        /*--------------------------------------------*/
        int sign_label = evaluate_frame_visual(frame);
//        gray = get_depth_frame();
        Mat depth = get_depth_frame();
        imshow("DEPTH", depth);
        log_color(frame);
        log_gray(depth);
        int bt_status = get_current_value();
//        int is_continue = control_car(steering_angle * 4.5, has_cross, sign_label, bt_status);
        int is_continue = control_car(steering_angle , has_cross,sign_label, bt_status);
        if (!is_continue) {
            break;
        }
        waitKey(1);
        double end = getTickCount();
        double fps = getTickFrequency()/(end-start);
        // cout<<endl<<fps<<endl;
    }

    release_video_writer();

    return 0;
}

