#include "lane_utils.h"
#include "cross_detection.h"

double upper_thresh_slope = 45;
double lower_thresh_slope = 10;

bool cross_check(Mat src) {
    Mat firstHalf, secondHalf;
    //ROI
    //Second ROI to divide the frame into 2 parts
    //First Half
    Point offsetF = Point(0, 0);
    int vectFirst[] = {offsetF.x, offsetF.y, src.cols / 2 - 20, src.rows};
    firstHalf = ROI(src, vectFirst);

    //Second Half
    Point offsetS = Point(firstHalf.cols, 0);
    int vectSecond[] = {offsetS.x, offsetS.y, src.cols - firstHalf.cols, src.rows};
    secondHalf = ROI(src, vectSecond);


    /*Variable for contour processing*/
    vector<vector<Point> > firstContours, secondContours;
    vector<Point> firstGroup;
    vector<Point> secondGroup;

    Vec4f f, s;
    double alphaF, alphaS;
    bool isCross = false;
    /*-------------------------------*/
    /*Contour processing*/
    firstContours = contoursGet(firstHalf);
    secondContours = contoursGet(secondHalf);
    /*-------------------------------*/
    /*Contours group*/
    firstGroup = contourFilter(firstContours);
    secondGroup = contourFilter(secondContours);

    for (int i = 0; i < firstGroup.size(); i++) {
        circle(firstHalf, firstGroup.at(i), 3, Scalar(0, 0, 255), 1, LINE_8);
    }
    for (int i = 0; i < secondGroup.size(); i++) {
        circle(secondHalf, secondGroup.at(i), 3, Scalar(0, 0, 255), 1, LINE_8);
    }
    /*-------------------------------*/
    /*Fit Line processing*/
    if (firstContours.size() != 0) {
        f = lineGet(firstGroup);
        alphaF = getSlope(f);
    }
    if (secondContours.size() != 0) {
        s = lineGet(secondGroup);
        alphaS = getSlope(s);
    }
    /*-------------------------------*/
    //    /*Draw Line*/
    //    firstHalf = drawLine(firstHalf, f, Point(-1, -1));
    //    secondHalf = drawLine(secondHalf, s, Point(-1, -1));
    /*-------------------------------*/
    if ((alphaS < upper_thresh_slope && alphaS > lower_thresh_slope)&&
            (alphaF > -upper_thresh_slope && alphaF < -lower_thresh_slope)) {

        //        firstHalf = drawLine(firstHalf, f, Point(-1, -1));
        //        secondHalf = drawLine(secondHalf, s, Point(-1, -1));

        isCross = true;
        waitKey(0);
    }

    //IMSHOW
    imshow("F", firstHalf);
    imshow("S", secondHalf);
    return isCross;
}

bool cross_check_visual(Mat src) {
    Mat firstHalf, secondHalf;
    //ROI
    //Second ROI to divide the frame into 2 parts
    //First Half
    Point offsetF = Point(0, 0);
    int vectFirst[] = {offsetF.x, offsetF.y, src.cols / 2 - 20, src.rows};
    firstHalf = ROI(src, vectFirst);

    //Second Half
    Point offsetS = Point(firstHalf.cols, 0);
    int vectSecond[] = {offsetS.x, offsetS.y, src.cols - firstHalf.cols, src.rows};
    secondHalf = ROI(src, vectSecond);


    /*Variable for contour processing*/
    vector<vector<Point> > firstContours, secondContours;
    vector<Point> firstGroup;
    vector<Point> secondGroup;

    Vec4f f, s;
    double alphaF, alphaS;
    bool isCross = false;
    /*-------------------------------*/
    /*Contour processing*/
    firstContours = contoursGet(firstHalf);
    secondContours = contoursGet(secondHalf);
    /*-------------------------------*/
    /*Contours group*/
    firstGroup = contourFilter(firstContours);
    secondGroup = contourFilter(secondContours);

    for (int i = 0; i < firstGroup.size(); i++) {
        circle(firstHalf, firstGroup.at(i), 3, Scalar(0, 0, 255), 1, LINE_8);
    }
    for (int i = 0; i < secondGroup.size(); i++) {
        circle(secondHalf, secondGroup.at(i), 3, Scalar(0, 0, 255), 1, LINE_8);
    }
    /*-------------------------------*/
    /*Fit Line processing*/
    if (firstContours.size() != 0) {
        f = lineGet(firstGroup);
        alphaF = getSlope(f);
    }
    if (secondContours.size() != 0) {
        s = lineGet(secondGroup);
        alphaS = getSlope(s);
    }
    /*-------------------------------*/
    //    /*Draw Line*/
    //    firstHalf = drawLine(firstHalf, f, Point(-1, -1));
    //    secondHalf = drawLine(secondHalf, s, Point(-1, -1));
    /*-------------------------------*/
    if ((alphaS < upper_thresh_slope && alphaS > lower_thresh_slope)&&
            (alphaF > -upper_thresh_slope && alphaF < -lower_thresh_slope)) {

        //        firstHalf = drawLine(firstHalf, f, Point(-1, -1));
        //        secondHalf = drawLine(secondHalf, s, Point(-1, -1));

        isCross = true;
        waitKey(0);
    }

    //IMSHOW
    imshow("F", firstHalf);
    imshow("S", secondHalf);
    return isCross;
}
