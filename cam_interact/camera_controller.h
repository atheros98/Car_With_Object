/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   camera_controller.h
 * Author: atheros
 *
 * Created on March 16, 2018, 10:44 PM
 */

#ifndef CAMERA_CONTROLLER_H
#define CAMERA_CONTROLLER_H

//add library here

#include <opencv2/opencv.hpp>
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

#include "stdio.h"
#include <iomanip>
#include <OpenNI.h>
#include <termios.h>
#include <fstream>
#include <iostream>

using namespace std;
using namespace cv;
using namespace openni;


//define constant here

#define VIDEO_FRAME_WIDTH 320
#define VIDEO_FRAME_HEIGHT 240




//declare variable
//Size output_size = Size(VIDEO_FRAME_WIDTH, VIDEO_FRAME_HEIGHT);

extern VideoStream color_stream;
extern VideoStream depth_stream;
extern Device device;


//declare function prototype here


void init_device();
void init_color_stream();
void init_depth_stream();
Mat get_color_frame();
Mat get_depth_frame();
void camera_release();
//convert camera frame format to opencv Mat



#endif /* CAMERA_CONTROLLER_H */

