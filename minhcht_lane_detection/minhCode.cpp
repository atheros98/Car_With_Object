#include "minhCode.h"

 int VECT[4] = {0,100,320,140};
 int NEEDEDFRAME = 260;
 int BEGINF = 0;
 int CONS = 2;
 int SENST = 20;
 bool NEEDWRITE = false;
 int  CROSSSENT = 20;
 int preState = 0;
 int LANESENT = 50;

Mat birdViewTransform(Mat src){
    int expandRow = 5;
    int expandCol = 2;
    int biger;
    if (expandCol > expandRow){
        biger = expandCol;
    }
    else{
        biger = expandRow;
    }
    resize(src,src,Size(src.cols/biger,src.rows/biger));
    Mat border= Mat::zeros(src.rows*expandRow, src.cols*expandCol,  src.type());
    Point startP;
    startP.x = (border.cols-src.cols)/2;
    startP.y = 0;
    src.copyTo(border(Rect(startP.x,startP.y, src.cols, src.rows)));
    Point2f srcVer[4];
    srcVer[0] = Point((border.cols-src.cols)/2,0);
    srcVer[1] = Point((border.cols+src.cols)/2,0);
    srcVer[2] = Point((border.cols-src.cols)/2,src.rows);
    srcVer[3] = Point((border.cols+src.cols)/2,src.rows);
    Point2f dstVer[4];
    dstVer[0] = Point(0,0);
    dstVer[1] = Point(border.cols,0);
    dstVer[2] = Point((border.cols-src.cols)/2,border.rows);
    dstVer[3] = Point((border.cols+src.cols)/2,border.rows);

    Mat M = getPerspectiveTransform(srcVer, dstVer);
    Mat dst;
    warpPerspective(border, dst, M, dst.size(), INTER_LINEAR, BORDER_CONSTANT);
    resize(src,src,Size(src.cols*biger,src.rows*biger));
    return dst;
}
Mat MROI(Mat src, int vect[]){
    Mat roi;
    roi = src(Rect(vect[0], vect[1], vect[2], vect[3]));
    return roi;
}
Mat drawLine(Mat src,Mat raw){
    vector<Vec4i> lines;
    HoughLinesP(src,lines,0.8,CV_PI/180,40,20,60);

//    cout << lines.size();
    for( size_t i = 0; i < lines.size(); i++ )
    {
        Vec4i l = lines[i];
        line( raw, Point(l[0], l[1]), Point(l[2], l[3]), Scalar(255,255,255), 5, CV_AA);
    }
//    cout<<endl<<raw.channels()<<endl;
    //cv::cvtColor(raw,raw, COLOR_BGR2GRAY);

    return raw;
}
Mat drawCenter(Mat src){
    int rowN = src.rows;
    int colN = src.cols;
    Mat empty;
    empty = Mat::zeros(src.rows,src.cols,src.type());
    empty = src.clone();
    for (int y = rowN-1; y >= 0; y --){
        int left=-1;
        int right=-1;
        for (int x = 0; x < colN; x++){
            if (src.at<Vec3b>(y,x)[2]==255){
                if (left==-1){
                    left = x;
                    x = x + 20;
                }
                else{
                    right = x;
                }

            }
        }

        int middle=-1;
        if (left != -1 && right != -1){
            middle = (left+right)/2;
        }
        else{
            middle = -1;
        }
        if (middle==-1){
            continue;
        }
        Point middleP;
        middleP = Point(middle,y);
        int radius = 1;
        circle(empty, middleP, (int) radius,//Radius
                Scalar(0,255,0),
                (int) 1,//thick ness
                LINE_AA);
    }
    return empty;
}
vector<Mat> pipeLine(Mat img){
    Mat  dst;
    Mat gray;
    Mat edged;
    Mat filted;
    Mat empty;
    Mat centered;
    Mat drawed;
    vector< Mat > result;

    if (img.cols >= 200){
        dst = MROI(img,VECT);
    }

    dst = birdViewTransform(dst);
    GaussianBlur(dst,dst,Size(3,3),0,0);
    resize(dst,dst,Size(dst.cols/CONS,dst.rows/CONS));
    int vect[4] = {0,dst.rows/10,dst.cols,dst.rows/10*9};

    cvtColor(dst,gray,CV_BGR2GRAY);
    threshold(gray,filted,240,255,THRESH_BINARY);
    Canny(filted,edged,50,150,3);
    empty = Mat::zeros(dst.rows,dst.cols,dst.type());
    drawed = drawLine(filted,empty);
    centered = drawCenter(drawed);

    result.push_back(drawed);
    result.push_back(centered);
    return result;
}
char checkCross(Mat input){
    Mat tmp = input;
    /////Left-Right-Top-Bott/////////////
    vector<vector<Point> > pointSet = getExPoint(input);
    vector<Point> exLeft = pointSet[0];
    vector<Point> exRight= pointSet[1];
    vector<Point> exTop  = pointSet[2];
    vector<Point> exBott = pointSet[3];
    bool outLeft = false;
    bool outRight= false;
    for (size_t i=0; i < exBott.size();i++){
         if ((abs(exTop[i].y-exLeft[i].y) < SENST)  &&(abs(exTop[i].y-exRight[i].y) < SENST) && (abs(exRight[i].x-exLeft[i].x)>CROSSSENT)){
             return 1;
         }

    }
//&& (abs(exBott[i].x-exLeft[i].x-(exRight[i].x-exLeft[i].x)/2)<5)
    ///////////////////////////////////////////////////////////////////////////////////
    return 0;
}
char isCross(Mat input){

    int check = checkCross(input);
//    if (preState==2 && check == 0){
//            check = 2;
//        }
    preState = check;
    return check;
}
float angleCalculator(char stage,char sign,Mat drawed,Mat centered){
    float tmp;
    if (stage==0)
    if (isTwoLane(drawed)){

        tmp = straightCal(centered);

    }
    else{
        tmp = turnCal(drawed);
    }
    if (stage==2){

        tmp = crossCal(sign,drawed)*11*20/90;

    }
    int checker = keepCenter(drawed);
    if (checker*tmp>0){
        if (tmp<0){
            tmp = 10/9;
        }
        else{
            tmp = -10/9;
        }

    }
    return tmp;
}
bool isTwoLane(Mat drawed){
    vector<vector<Point> > pointSet = getExPoint(drawed);
    vector<Point> exLeft = pointSet[0];
    vector<Point> exRight= pointSet[1];
    vector<Point> exTop  = pointSet[2];
    vector<Point> exBott = pointSet[3];
    int radius = 3;
    char count=0;
    for (size_t i =0; i < exBott.size();i++){
        float disX = pow((exBott[i].x-exTop[i].x),2);
        float disY = pow((exBott[i].y-exTop[i].y),2);
        if (sqrt(disX+disY) > LANESENT){
            count++;
        }
    }
    if (count >=2){
        return true;
    }
    return false;
}

vector<vector<Point> > getExPoint(Mat input){
    //Mat gray;
    Mat filted=input.clone();
    vector<vector<Point> > cnts;
    vector<Vec4i> hierarchy;

    //cvtColor(input,gray,CV_BGR2GRAY);
    //threshold(gray,filted,50,255,THRESH_BINARY);
    findContours(filted,cnts,hierarchy,RETR_EXTERNAL,CHAIN_APPROX_SIMPLE,Point(0,0));

    vector<Point> exLeft;
    vector<Point> exRight;
    vector<Point> exTop;
    vector<Point> exBott;
    for (size_t i = 0; i < cnts.size();i++){
        Point tmpLeft;
        Point tmpRight;
        Point tmpTop;
        Point tmpBott;
        tmpLeft.x = 1000;
        tmpRight.x = -1000;
        tmpBott.y = -1000;
        tmpTop.y = 1000;

        vector<Point> cnt;
        cnt = cnts[i];
        for (size_t j=0; j < cnt.size();j++){
            Point current = cnt[j];
            if (current.x < tmpLeft.x){
                tmpLeft = current;
            }
            if (current.x > tmpRight.x){
                tmpRight = current;
            }
            if (current.y > tmpBott.y){
                tmpBott = current;
            }
            if (current.y < tmpTop.y){
                tmpTop = current;
            }
        }
        exLeft.push_back(tmpLeft);
        exRight.push_back(tmpRight);
        exTop.push_back(tmpTop);
        exBott.push_back(tmpBott);

    }
    vector<vector<Point> > result;
    result.push_back(exLeft);
    result.push_back(exRight);
    result.push_back(exTop);
    result.push_back(exBott);
    return result;
}
float angleByAtan2(Point first,Point second){
    int x =(first.x-second.x);
    int y =(first.y-second.y);
    float res = atan2(y,x);
    res = res/CV_PI*90;
    res = 45 - res;
    return res;
}
float straightCal(Mat input){
    vector<vector<Point> > pointSet = getExPoint(input);

    vector<Point> exLeft = pointSet[0];
    vector<Point> exRight= pointSet[1];
    vector<Point> exTop  = pointSet[2];
    vector<Point> exBott = pointSet[3];
    float res =  angleByAtan2(exBott[0],exTop[0]);
    return res;
}
float turnCal(Mat input){
    vector<vector<Point> > pointSet = getExPoint(input);
    vector<Point> exLeft = pointSet[0];
    vector<Point> exRight= pointSet[1];
    vector<Point> exTop  = pointSet[2];
    vector<Point> exBott = pointSet[3];
    int max = 0;
    Point top;
    Point bott;
    for (size_t i = 0; i < exBott.size();i++){
        if (abs(exBott[i].x-exTop[i].x) > max){
            max = abs(exBott[i].x-exTop[i].x);
            top = exTop[i];
            bott = exBott[i];
        }
    }
    float res = angleByAtan2(bott,top);
    return res;
}
float crossCal(char sign,Mat input){
    vector<vector<Point> > pointSet = getExPoint(input);
    vector<Point> exLeft = pointSet[0];
    vector<Point> exRight= pointSet[1];
    vector<Point> exTop  = pointSet[2];
    vector<Point> exBott = pointSet[3];
    int max = 0;
    int min = 10000;
    Point leftTop;
    Point leftBott;
    Point rightTop;
    Point rightBott;
    for (size_t i = 0; i < exBott.size();i++){
        if (exBott[i].x<min){
            leftBott = exBott[i];
            leftTop  = exTop[i];
        }
        if (exBott[i].x>max){
            rightBott = exBott[i];
            leftTop   = exTop[i];
        }
    }
    float res;
    if (sign == 1){
        res = angleByAtan2(leftBott,leftTop);
    }
    else{
        res = angleByAtan2(rightBott,rightTop);
    }
    return res;
}
float angleProcess(Mat centered,Mat drawed){

    float res = angleCalculator(0,1,drawed,centered);
    res = res*1.5/20*90;
    return res;
}
bool crossProcess(Mat drawed){
    int check =  isCross(drawed);
    if (check==1){
        return true;
    }
    return false;
}
float angleProcessVisual(Mat& check,Mat centered,Mat drawed){
    float res = angleProcess(centered,drawed);
    Point org;
    org.y = drawed.rows/5*2;
    org.x = drawed.cols/3;
    stringstream coverter;
    coverter << res;
    putText(check, coverter.str(), org, FONT_HERSHEY_SCRIPT_SIMPLEX, 1,
    Scalar(0,255,255), 3, 8);
    addWeighted(centered,0.5,check,0.5,0.0,check);
    imshow("CHECK CODE MINH",check);
    return res;
}
bool crossProcessVisual(Mat& check,Mat drawed){
    check = drawed.clone();
    bool res = crossProcess(drawed);
    if (res){
        Point org;
        org.y = drawed.rows/4*2;
        org.x = drawed.cols/3;
        putText(check, "|||||||", org, FONT_HERSHEY_SCRIPT_SIMPLEX, 1,
        Scalar(0,255,255), 3, 8);
    }
    imshow("check", check);
    return res;
}
int keepCenter(Mat drawed){
    vector<vector<Point> > pointSet = getExPoint(drawed);

    vector<Point> exLeft = pointSet[0];
    vector<Point> exRight= pointSet[1];
    vector<Point> exTop  = pointSet[2];
    vector<Point> exBott = pointSet[3];
    for (size_t i=0; i < exBott.size();i++){
        if (exBott[i].x<drawed.cols/2 && exBott[i].x > drawed.cols/3){
            return 1;
        }
        if (exBott[i].x>drawed.cols/2 && exBott[i].x < drawed.cols/3*2){
            return -1;
        }
    }
    return 0;
}
