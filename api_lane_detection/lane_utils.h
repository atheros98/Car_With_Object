#ifndef LANE_UTILS_H
#define LANE_UTILS_H
#include "opencv2/imgcodecs.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/highgui.hpp"
#include <iostream>
#include <fstream>
using namespace std;
using namespace cv;

/*Define constants*/
const int WIDTH = 320;
const int HEIGHT = 240;
const int thresh_day = 240;
const int thresh_night = 170;
/*Support functions*/
Mat ROI(Mat src, int vect[]);
//Mat smoothImage(Mat src);
Mat grayScale(Mat src);
Mat colorFilter(Mat src);
Mat contoursMat(Mat src);
vector<Point> contourFilter(vector<vector<Point> > contours);
vector<vector<Point> > contoursGet(Mat src);
vector<Point> momentGet(vector<vector<Point> > moment);
Vec4f lineGet(vector<Point> contour);
Vec4f functionGet(Vec4f l);
Point intersectOfTwoFunc(Vec4f f, Vec4f s);
Mat drawLine(Mat src, Vec4f l, Point lastPoint);
double getSlope(Vec4f l);
Mat putTextOnMat(Mat src, double number, Point position );
Mat birdEyeView(Mat src);
Mat drawLANE(Mat src, Vec4f l);
double getSteeringAngle(double alpha, double beta);
double reverseAng(double value);



#endif /* LANE_UTILS_H */

