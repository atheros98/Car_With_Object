#include "lane_utils.h"
#include "lane_detection.h"


double steering_angle=0;
Point center;
bool startedSet = false;
int halfRoadWidth = 0;
Point preCenter;
double lane_steering(Mat src) {
    /*Detect on two halves of frame */
    Mat firstHalf, secondHalf;
    //ROI
    //Second ROI to divide the frame into 2 parts
    //First Half
    Point offsetF = Point(0, 0);
    int vectFirst[] = {offsetF.x, offsetF.y, src.cols / 2 - 20, src.rows};
    firstHalf = ROI(src, vectFirst);

    //Second Half
    Point offsetS = Point(firstHalf.cols, 0);
    int vectSecond[] = {offsetS.x, offsetS.y, src.cols - firstHalf.cols, src.rows};
    secondHalf = ROI(src, vectSecond);

    /*Contour processing*/
    /*Variable for contour processing*/
    vector<vector<Point> > firstContours, secondContours;
    vector<Point> firstGroup, secondGroup;
    Vec4f f, s;
    double alphaF, alphaS;
    bool isCross = false;
    /*-------------------------------*/

    /*Contours Processing*/
    firstContours = contoursGet(firstHalf);
    secondContours = contoursGet(secondHalf);
    /*-------------------------------*/

    /*Contours group*/
    firstGroup = contourFilter(firstContours);
    secondGroup = contourFilter(secondContours);
    /*-------------------------------*/

    /*Fit Line*/
    if (firstContours.size() != 0) {
        f = lineGet(firstGroup);
    }
    if (secondContours.size() != 0) {
        s = lineGet(secondGroup);
        s[2] = s[2] + firstHalf.cols;
        s[3] = s[3];
    }
    /*-------------------------------*/
    /*Get slope*/
    alphaF = getSlope(f);
    alphaS = getSlope(s);
    /*-------------------------------*/

    /*Get steering angle*/
    Mat dst;
    dst = Mat::zeros(src.size(), CV_8UC3);
    if (!std::isnan(alphaF) || !std::isnan(alphaS)) {
        steering_angle = getSteeringAngle(alphaF, alphaS);
    }
    /*-------------------------------*/

    return steering_angle;
}

double lane_steering_visual(Mat src, Mat &refFrame) {
    steering_angle = 0;
    /*Detect on two halves of frame */
    Mat firstHalf, secondHalf;
    //ROI
    //Second ROI to divide the frame into 2 parts
    //First Half
    Point offsetF = Point(0, 0);
    int vectFirst[] = {offsetF.x, offsetF.y, src.cols / 2 - 20, src.rows};
    firstHalf = ROI(src, vectFirst);

    //Second Half
    Point offsetS = Point(firstHalf.cols, 0);
    int vectSecond[] = {offsetS.x, offsetS.y, src.cols - firstHalf.cols, src.rows};
    secondHalf = ROI(src, vectSecond);

    /*Contour processing*/
    /*Variable for contour processing*/
    vector<vector<Point> > firstContours, secondContours;
    vector<Point> firstGroup, secondGroup;
    Vec4f f, s;
    double alphaF, alphaS;
    bool isCross = false;
    /*-------------------------------*/

    /*Contours Processing*/
    firstContours = contoursGet(firstHalf);
    secondContours = contoursGet(secondHalf);
    /*-------------------------------*/

    /*Contours group*/
    firstGroup = contourFilter(firstContours);
    secondGroup = contourFilter(secondContours);
    for (int i = 0; i < firstGroup.size(); i++) {
        circle(firstHalf, firstGroup.at(i), 3, Scalar(0, 0, 255), 1, LINE_8);
    }
    for (int i = 0; i < secondGroup.size(); i++) {
        circle(secondHalf, secondGroup.at(i), 3, Scalar(0, 0, 255), 1, LINE_8);
    }
    /*-------------------------------*/

    /*Fit Line*/
    if (firstContours.size() != 0) {
        f = lineGet(firstGroup);
    }
    if (secondContours.size() != 0) {
        s = lineGet(secondGroup);
        s[2] = s[2] + firstHalf.cols;
        s[3] = s[3];
    }
    /*-------------------------------*/
    /*Get slope*/
    alphaF = getSlope(f);
    alphaS = getSlope(s);
    /*-------------------------------*/

    /*Get steering angle*/
    Mat dst;
    dst = Mat::zeros(src.size(), CV_8UC3);
    if (!std::isnan(alphaF) || !std::isnan(alphaS)) {
        steering_angle = getSteeringAngle(alphaF, alphaS);
    }
    /*-------------------------------*/

    /*Visual if needed*/
    char angle[30];

    sprintf(angle, "F=%f", alphaF);
    putText(dst, angle, Point(100, 80), FONT_HERSHEY_SIMPLEX, .7, Scalar(0, 0, 255), 2, 8, false);
    sprintf(angle, "S=%f", alphaS);
    putText(dst, angle, Point(100, 100), FONT_HERSHEY_SIMPLEX, .7, Scalar(0, 0, 255), 2, 8, false);


    sprintf(angle, "X=%f", steering_angle);
    putText(dst, angle, Point(100, 60), FONT_HERSHEY_SIMPLEX, .7, Scalar(0, 0, 255), 2, 8, false);

    dst = drawLANE(dst, f);
    dst = drawLANE(dst, s);
    /*------------------*/
    refFrame = dst;

    return steering_angle;
}
/*-----------------------------------------------*/
double laneGetCenter(Mat src) {
    Mat dst;
    dst = Mat::zeros(src.size(), CV_8UC3);
    /*Detect on two halves of frame */
    Mat firstHalf, secondHalf;
    //ROI
    //Second ROI to divide the frame into 2 parts
    //First Half
    Point offsetF = Point(0, 0);
    int vectFirst[] = {offsetF.x, offsetF.y, src.cols / 2 - 20, src.rows};
    firstHalf = ROI(src, vectFirst);

    //Second Half
    Point offsetS = Point(firstHalf.cols, 0);
    int vectSecond[] = {offsetS.x, offsetS.y, src.cols - firstHalf.cols, src.rows};
    secondHalf = ROI(src, vectSecond);

    /*Contour processing*/
    /*Variable for contour processing*/
    vector<vector<Point> > firstContours, secondContours;
    vector<Point> firstGroup, secondGroup;
    Vec4f f, s;
    double alphaF, alphaS;
    bool isCross = false;
    /*-------------------------------*/

    /*Contours Processing*/
    firstContours = contoursGet(firstHalf);
    secondContours = contoursGet(secondHalf);
    /*-------------------------------*/

    /*Contours group*/
    firstGroup = contourFilter(firstContours);
    secondGroup = contourFilter(secondContours);
    /*-------------------------------*/

    /*Fit Line*/
    if (firstContours.size() != 0) {
        f = lineGet(firstGroup);
    }
    if (secondContours.size() != 0) {
        s = lineGet(secondGroup);
        s[2] = s[2] + firstHalf.cols;
        s[3] = s[3];
    }
    /*-------------------------------*/

    /*Get slope*/
    alphaF = getSlope(f);
    alphaS = getSlope(s);
    /*-------------------------------*/

    /*Logic processing*/
    if (!std::isnan(alphaF) && !std::isnan(alphaS)) {
        double diff = std::abs(alphaF - alphaS);
        if ((diff > 0 && diff < 25)&&(alphaF < 0 && alphaS < 0)) {
            alphaF = (alphaF + alphaS) / 2;
            alphaS = (alphaF + alphaS) / 2;
            dst = drawLANE(dst, f);
        } else if ((diff > 0 && diff < 25)&&(alphaF > 0 && alphaS > 0)) {
            alphaF = (alphaF + alphaS) / 2;
            alphaS = (alphaF + alphaS) / 2;
            dst = drawLANE(dst, s);
        } else {
            dst = drawLANE(dst, f);
            dst = drawLANE(dst, s);
        }
    } else {
        dst = drawLANE(dst, f);
        dst = drawLANE(dst, s);
    }
    imshow("ORI", dst);
    /*-------------------------------*/

    /*Get center processing*/
    cvtColor(dst, dst, COLOR_BGR2GRAY);
    Mat newDst;
    newDst = Mat::zeros(dst.size(), CV_8UC3);

    //    newDst = drawLANE(newDst, f);
    //    newDst = drawLANE(newDst, s);


    int y = dst.rows / 3;
    int leftStart, rightStart;
    leftStart = -1;
    rightStart = -1;
    if (!std::isnan(alphaF) && !std::isnan(alphaS)) {
        for (int x = 0; x < dst.cols; x++) {
            if (dst.at<uchar>(y, x) == 255) {
                leftStart = x;
                break;
            }
        }
        if (leftStart != -1) {
            for (int x = leftStart + 10; x < dst.cols; x++) {
                if (dst.at<uchar>(y, x) == 255) {
                    rightStart = x;
                    break;
                }
            }
        }
        startedSet = true;
        halfRoadWidth = rightStart - leftStart;
        center.x = (rightStart + leftStart) / 2;
        center.y = y;
    } else {
        if (std::isnan(alphaF)) {
            for (int x = 0; x < dst.cols; x++) {
                if (dst.at<uchar>(y, x) == 255) {
                    rightStart = x;
                    break;
                }
            }
            if (startedSet) {

            }
            center.x = rightStart - halfRoadWidth / 2;
            center.y = y;
        } else if (std::isnan(alphaS)) {
            for (int x = 0; x < dst.cols; x++) {
                if (dst.at<uchar>(y, x) == 255) {
                    leftStart = x;
                    break;
                }
            }

            center.x = leftStart + halfRoadWidth / 2;
            center.y = y;
        }
    }

    if (center.x > 0) {
        double slope;
        slope = getTheta(center, Point(WIDTH / 2, HEIGHT));
        steering_angle = slope;
    }

    /*-------------------------------*/
    return steering_angle;
}
double lane_detection_center_visual(Mat src, Mat &refFrame) {
    Mat dst;
    dst = Mat::zeros(src.size(), CV_8UC3);
    /*Detect on two halves of frame */
    Mat firstHalf, secondHalf;
    //ROI
    //Second ROI to divide the frame into 2 parts
    //First Half
    Point offsetF = Point(0, 0);
    int vectFirst[] = {offsetF.x, offsetF.y, src.cols / 2 - 20, src.rows};
    firstHalf = ROI(src, vectFirst);

    //Second Half
    Point offsetS = Point(firstHalf.cols, 0);
    int vectSecond[] = {offsetS.x, offsetS.y, src.cols - firstHalf.cols, src.rows};
    secondHalf = ROI(src, vectSecond);

    /*Contour processing*/
    /*Variable for contour processing*/
    vector<vector<Point> > firstContours, secondContours;
    vector<Point> firstGroup, secondGroup;
    Vec4f f, s;
    double alphaF, alphaS;
    bool isCross = false;
    /*-------------------------------*/

    /*Contours Processing*/
    firstContours = contoursGet(firstHalf);
    secondContours = contoursGet(secondHalf);
    /*-------------------------------*/

    /*Contours group*/
    firstGroup = contourFilter(firstContours);
    secondGroup = contourFilter(secondContours);
    /*-------------------------------*/

    /*Fit Line*/
    if (firstContours.size() != 0) {
        f = lineGet(firstGroup);
    }
    if (secondContours.size() != 0) {
        s = lineGet(secondGroup);
        s[2] = s[2] + firstHalf.cols;
        s[3] = s[3];
    }
    /*-------------------------------*/

    /*Get slope*/
    alphaF = getSlope(f);
    alphaS = getSlope(s);
    /*-------------------------------*/

    /*Logic processing*/
    if (!std::isnan(alphaF) && !std::isnan(alphaS)) {
        double diff = std::abs(alphaF - alphaS);
        if ((diff > 0 && diff < 25)&&(alphaF < 0 && alphaS < 0)) {
            alphaF = (alphaF + alphaS) / 2;
            alphaS = (alphaF + alphaS) / 2;
            dst = drawLANE(dst, f);
        } else if ((diff > 0 && diff < 25)&&(alphaF > 0 && alphaS > 0)) {
            alphaF = (alphaF + alphaS) / 2;
            alphaS = (alphaF + alphaS) / 2;
            dst = drawLANE(dst, s);
        } else {
            dst = drawLANE(dst, f);
            dst = drawLANE(dst, s);
        }
    } else {
        dst = drawLANE(dst, f);
        dst = drawLANE(dst, s);
    }
    imshow("ORI", dst);
    /*-------------------------------*/

    /*Get center processing*/
    cvtColor(dst, dst, COLOR_BGR2GRAY);
    Mat newDst;
    newDst = Mat::zeros(dst.size(), CV_8UC3);

    //    newDst = drawLANE(newDst, f);
    //    newDst = drawLANE(newDst, s);


    int y = dst.rows / 3;
    int leftStart, rightStart;
    leftStart = -1;
    rightStart = -1;
    if (!std::isnan(alphaF) && !std::isnan(alphaS)) {
        for (int x = 0; x < dst.cols; x++) {
            if (dst.at<uchar>(y, x) == 255) {
                leftStart = x;
                break;
            }
        }
        if (leftStart != -1) {
            for (int x = leftStart + 10; x < dst.cols; x++) {
                if (dst.at<uchar>(y, x) == 255) {
                    rightStart = x;
                    break;
                }
            }
        }
        startedSet = true;
        halfRoadWidth = rightStart - leftStart;
        center.x = (rightStart + leftStart) / 2;
        center.y = y;
    } else {
        if (std::isnan(alphaF)) {
            for (int x = 0; x < dst.cols; x++) {
                if (dst.at<uchar>(y, x) == 255) {
                    rightStart = x;
                    break;
                }
            }
            if (startedSet) {

            }
            center.x = rightStart - halfRoadWidth / 2;
            center.y = y;
        } else if (std::isnan(alphaS)) {
            for (int x = 0; x < dst.cols; x++) {
                if (dst.at<uchar>(y, x) == 255) {
                    leftStart = x;
                    break;
                }
            }

            center.x = leftStart + halfRoadWidth / 2;
            center.y = y;
        }
    }
    circle(newDst, Point(leftStart, y), 3, Scalar(255, 0, 0), 1, LINE_8);
    circle(newDst, Point(rightStart, y), 3, Scalar(0, 255, 0), 1, LINE_8);
    circle(newDst, center, 3, Scalar(0, 0, 255), 1, LINE_8);
    cout << endl << center.x << "," << center.y << endl;

    if (center.x > 0) {
        double slope;
        slope = getTheta(center, Point(WIDTH / 2, HEIGHT));
        steering_angle = slope;
    }
    newDst = drawLANE(newDst,f);
    newDst = drawLANE(newDst,s);

    char angle[30];

    sprintf(angle, "F=%f", steering_angle);
    putText(newDst, angle, Point(100, 120), FONT_HERSHEY_SIMPLEX, .7, Scalar(0, 0, 255), 2, 8, false);
    sprintf(angle, "L=%d", leftStart);
    putText(newDst, angle, Point(100, 60), FONT_HERSHEY_SIMPLEX, .7, Scalar(0, 0, 255), 2, 8, false);
    sprintf(angle, "R=%d", rightStart);
    putText(newDst, angle, Point(100, 40), FONT_HERSHEY_SIMPLEX, .7, Scalar(0, 0, 255), 2, 8, false);
    sprintf(angle, "X=%d", center.x);
    putText(newDst, angle, Point(100, 80), FONT_HERSHEY_SIMPLEX, .7, Scalar(0, 0, 255), 2, 8, false);
    /*-------------------------------*/
    refFrame=newDst;
    return steering_angle;
}

double getSlopeThroughTwoPoints(Point first, Point second) {
    double slope = 0;
    double pi = 3.141592;
    if (first.x != second.x) {
        slope = atan((first.y - second.y) / (first.x - second.x));
        slope = slope * 180 / pi;
    }

    return slope;
}
double getTheta(Point car, Point dst) {
    if (dst.x == car.x) return 0;
    if (dst.y == car.y) return (dst.x < car.x ? -90 : 90);
    double pi = acos(-1.0);
    double dx = dst.x - car.x;
    double dy = car.y - dst.y; // image coordinates system: car.y > dst.y
    if (dx < 0) return atan(-dx / dy) * 180 / pi;
    return -atan(dx / dy) * 180 / pi;
}
/*Third*/
double lane_get_contour(Mat src){

    cout<<endl<<src.channels()<<endl;
    Mat dst;
    dst = Mat::zeros(src.clone().rows, src.clone().cols, CV_8UC3);
    for (int y = 0; y < src.rows; y++)
        for (int x = 0; x < src.cols; x++) {
            if (src.at<uchar>(y, x) == 255) {
                dst.at<Vec3b>(y, x)[0] = 255;
                dst.at<Vec3b>(y, x)[1] = 255;
                dst.at<Vec3b>(y, x)[2] = 255;
            }

        }

    int y = src.rows/3;
    int leftStart, rightStart;
    leftStart = -1;
    rightStart = -1;

    Point left_start = Point(-1, -1);
    Point left_end = Point(-1, -1);
    Point right_start = Point(-1, -1);
    Point right_end = Point(-1, -1);
    Point center = Point(-1, -1);


    bool met = false;
    int count_black = 0;

    for (int x = 0; x < src.cols; x++) {
        if (src.at<uchar>(y, x) == 255 && !met) {
            left_start.x = x;
            left_start.y = y;
            met = true;
        } else if (src.at<uchar>(y, x) == 0 && met) {
            count_black++;
            if (count_black >= 20) {
                left_end.x = x - 20;
                left_end.y = y;
                met = false;
                break;
            }
        } else if (src.at<uchar>(y, x) == 255 && met) {
            count_black = 0;
        }
    }

    circle(dst, left_start, 3, Scalar(255, 0, 0), 3, LINE_8);
    circle(dst, left_end, 3, Scalar(0, 255, 0), 3, LINE_8);


    for (int x = left_end.x + 50; x < src.cols; x++) {
        if (src.at<uchar>(y, x) == 255) {
            right_start.x = x;
            right_start.y = y;
            break;
        }
    }

    circle(dst, right_start, 3, Scalar(0, 255, 0), 3, LINE_8);

    if (left_end.x != -1 && right_start.x != -1) {
        center.x = (int) (left_end.x + right_start.x) / 2;
        center.y = y;
        preCenter = center;
        halfRoadWidth = center.x - left_end.x;
    } else if (right_start.x == -1) {
        if (preCenter.x > left_end.x) {
            center.x = left_end.x + halfRoadWidth;
            center.y = y;
            preCenter = center;
        } else {
            center.x = left_end.x - halfRoadWidth;
            center.y = y;
            preCenter = center;
        }
    } else { //khong bat duoc 2 lane
        center = preCenter;
    }
    circle(dst, center, 3, Scalar(0, 0, 255), 3, LINE_8);
    steering_angle = getTheta(center,Point(WIDTH/2, HEIGHT));
    return steering_angle;
}
double lane_get_contour_visual(Mat src, Mat &refFrame){
        Point offset = Point(0, 80);
    int vect[] = {offset.x, offset.y, WIDTH, HEIGHT - offset.y};
    //Process with the whole frame
    src = ROI(src, vect);

    //cout<<endl<<src.channels()<<endl;
    Mat dst;
    dst = Mat::zeros(src.clone().rows, src.clone().cols, CV_8UC3);
    for (int y = 0; y < src.rows; y++)
        for (int x = 0; x < src.cols; x++) {
            if (src.at<uchar>(y, x) == 255) {
                dst.at<Vec3b>(y, x)[0] = 255;
                dst.at<Vec3b>(y, x)[1] = 255;
                dst.at<Vec3b>(y, x)[2] = 255;
            }

        }

    int y = src.rows/3;
    int leftStart, rightStart;
    leftStart = -1;
    rightStart = -1;

    Point left_start = Point(-1, -1);
    Point left_end = Point(-1, -1);
    Point right_start = Point(-1, -1);
    Point right_end = Point(-1, -1);
    Point center = Point(-1, -1);


    bool met = false;
    int count_black = 0;

    for (int x = 0; x < src.cols; x++) {
        if (src.at<uchar>(y, x) == 255 && !met) {
            left_start.x = x;
            left_start.y = y;
            met = true;
        } else if (src.at<uchar>(y, x) == 0 && met) {
            count_black++;
            if (count_black >= 20) {
                left_end.x = x - 20;
                left_end.y = y;
                met = false;
                break;
            }
        } else if (src.at<uchar>(y, x) == 255 && met) {
            count_black = 0;
        }
    }

    circle(dst, left_start, 3, Scalar(255, 0, 0), 3, LINE_8);
    circle(dst, left_end, 3, Scalar(0, 255, 0), 3, LINE_8);


    for (int x = left_end.x + 120; x < src.cols; x++) {
        if (src.at<uchar>(y, x) == 255) {
            right_start.x = x;
            right_start.y = y;
            break;
        }
    }

    circle(dst, right_start, 3, Scalar(0, 255, 0), 3, LINE_8);

    if (left_end.x != -1 && right_start.x != -1) {
        center.x = (int) (left_end.x + right_start.x) / 2;
        center.y = y;
        preCenter = center;
        halfRoadWidth = center.x - left_end.x;
    } else if (right_start.x == -1) {
        if (preCenter.x > left_end.x) {
            center.x = left_end.x + halfRoadWidth;
            center.y = y;
            preCenter = center;
        } else {
            center.x = left_end.x - halfRoadWidth;
            center.y = y;
            preCenter = center;
        }
    } else { //khong bat duoc 2 lane
        center = preCenter;
    }
    circle(dst, center, 3, Scalar(0, 0, 255), 3, LINE_8);
    steering_angle = getTheta(center,Point(WIDTH/2, HEIGHT));
    imshow("DST",dst);
    refFrame = dst;
    return steering_angle;
}
