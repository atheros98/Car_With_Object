#include "driving_controller.h"

PCA9685 *pca9685 = new PCA9685();

int throttle_value;
double steer_angle;
bool running;
bool stopped;
bool printed = false;

int init_engine(int init_throttle) {
    steer_angle = 0;
    running = false;
    stopped = false;
    api_pwm_pca9685_init( pca9685 );
    int throttle = 0;
    api_set_FORWARD_control(pca9685, throttle);
    throttle_value = init_throttle;
    cout << "Initial throttle: " << throttle_value << endl;
}
int control_car(double angle, bool& has_cross, int& sign_label, int bt_status) {
    char key = getkey();
    switch (bt_status) {
    case 1:
        break;
    case 2:
        running = !running;
        printed = false;
        break;
    case 3:
        stopped = true;
        break;
    case 4:
        break;

    }

    if (key == 's') {
        running = !running;
        printed = false;
    } else if (key == 'f') {
        stopped = true;
    }

    if (stopped) {
        cout << "End process" << endl;
        car_stop();
        return 0;
    }

    if (running) {
        if (!printed) {
            cout << "ON" << endl;
            printed = true;
        }
        const double angle_shift = 90;
        if (has_cross && sign_label!= -1) {
            if (sign_label == 0) {
                steer_angle = angle + angle_shift;
            } else if (sign_label == 1) {
                steer_angle = angle - angle_shift;
            }

        } else {
            steer_angle = angle;
        }
        car_run();

        if (has_cross && sign_label != -1){
            waitKey(500);
        }
        return 1;
    } else {
        if (!printed) {
            cout << "OFF" << endl;
            printed = true;
        }
        car_pause();
        return 1;
    }

}

int control_car(double angle, bool has_cross, int bt_status) {
    char key = getkey();
    switch (bt_status) {
    case 1:
        break;
    case 2:
        running = !running;
        printed = false;
        break;
    case 3:
        stopped = true;
        break;
    case 4:
        break;

    }

    if (key == 's') {
        running = !running;
        printed = false;
    } else if (key == 'f') {
        stopped = true;
    }

    if (stopped) {
        cout << "End process" << endl;
        car_stop();
        return 0;
    }

    if (running) {
        if (!printed) {
            cout << "ON" << endl;
            printed = true;
        }
        const double angle_shift = 90;
        if (has_cross ) {
            steer_angle = angle+angle_shift;
            //has_cross = false;
            cout<<"fuck cross"<<endl;
            //car_pause();
        } else {
            steer_angle = angle;
        }
        car_run();
        if(has_cross) {
            waitKey(500);

        }

        return 1;
    } else {
        if (!printed) {
            cout << "OFF" << endl;
            printed = true;
        }
        car_pause();
        return 1;
    }

}


void car_run() {
    int throttle = throttle_value;
    api_set_STEERING_control(pca9685, steer_angle);
    // cout << steer_angle << endl;
    api_set_FORWARD_control(pca9685, throttle_value);
}

void car_pause() {
    int throttle = 0;
    api_set_FORWARD_control(pca9685, throttle);
}

void car_stop() {
    steer_angle = 0;
    int throttle = 0;
    api_set_STEERING_control(pca9685, steer_angle);
    api_set_FORWARD_control(pca9685, throttle);
}
