#ifndef VIDEO_LOGGER_H_INCLUDED
#define VIDEO_LOGGER_H_INCLUDED

#include <opencv2/opencv.hpp>
using namespace cv;
using namespace std;

const string color_name = "/home/ubuntu/ControlCar/video/color.avi";
const string gray_name = "/home/ubuntu/ControlCar/video/gray.avi";

extern VideoWriter color_writer;
extern VideoWriter gray_writer;
extern int codec;

void init_video_writer(Size gray_video_size);
void log_color(Mat color);
void log_gray(Mat gray);
void release_video_writer();

#endif // VIDEO_LOGGER_H_INCLUDED
