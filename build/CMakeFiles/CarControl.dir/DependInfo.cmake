# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/ubuntu/ControlCar/CarController.cpp" "/home/ubuntu/ControlCar/build/CMakeFiles/CarControl.dir/CarController.cpp.o"
  "/home/ubuntu/ControlCar/GPIO/Hal.cpp" "/home/ubuntu/ControlCar/build/CMakeFiles/CarControl.dir/GPIO/Hal.cpp.o"
  "/home/ubuntu/ControlCar/GPIO/gpio_controller.cpp" "/home/ubuntu/ControlCar/build/CMakeFiles/CarControl.dir/GPIO/gpio_controller.cpp.o"
  "/home/ubuntu/ControlCar/api_lane_detection/cross_detection.cpp" "/home/ubuntu/ControlCar/build/CMakeFiles/CarControl.dir/api_lane_detection/cross_detection.cpp.o"
  "/home/ubuntu/ControlCar/api_lane_detection/frame_processing.cpp" "/home/ubuntu/ControlCar/build/CMakeFiles/CarControl.dir/api_lane_detection/frame_processing.cpp.o"
  "/home/ubuntu/ControlCar/api_lane_detection/lane_detection.cpp" "/home/ubuntu/ControlCar/build/CMakeFiles/CarControl.dir/api_lane_detection/lane_detection.cpp.o"
  "/home/ubuntu/ControlCar/api_lane_detection/lane_utils.cpp" "/home/ubuntu/ControlCar/build/CMakeFiles/CarControl.dir/api_lane_detection/lane_utils.cpp.o"
  "/home/ubuntu/ControlCar/cam_interact/camera_controller.cpp" "/home/ubuntu/ControlCar/build/CMakeFiles/CarControl.dir/cam_interact/camera_controller.cpp.o"
  "/home/ubuntu/ControlCar/driving_control/driving_controller.cpp" "/home/ubuntu/ControlCar/build/CMakeFiles/CarControl.dir/driving_control/driving_controller.cpp.o"
  "/home/ubuntu/ControlCar/driving_control/peripheral_driver/api_i2c_pwm.cpp" "/home/ubuntu/ControlCar/build/CMakeFiles/CarControl.dir/driving_control/peripheral_driver/api_i2c_pwm.cpp.o"
  "/home/ubuntu/ControlCar/driving_control/peripheral_driver/pca9685.cpp" "/home/ubuntu/ControlCar/build/CMakeFiles/CarControl.dir/driving_control/peripheral_driver/pca9685.cpp.o"
  "/home/ubuntu/ControlCar/minhcht_lane_detection/minhCode.cpp" "/home/ubuntu/ControlCar/build/CMakeFiles/CarControl.dir/minhcht_lane_detection/minhCode.cpp.o"
  "/home/ubuntu/ControlCar/traffic_sign/traffic_sign_classify.cpp" "/home/ubuntu/ControlCar/build/CMakeFiles/CarControl.dir/traffic_sign/traffic_sign_classify.cpp.o"
  "/home/ubuntu/ControlCar/traffic_sign/traffic_sign_detection.cpp" "/home/ubuntu/ControlCar/build/CMakeFiles/CarControl.dir/traffic_sign/traffic_sign_detection.cpp.o"
  "/home/ubuntu/ControlCar/traffic_sign/traffic_sign_recognition.cpp" "/home/ubuntu/ControlCar/build/CMakeFiles/CarControl.dir/traffic_sign/traffic_sign_recognition.cpp.o"
  "/home/ubuntu/ControlCar/video_logger/video_logger.cpp" "/home/ubuntu/ControlCar/build/CMakeFiles/CarControl.dir/video_logger/video_logger.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS
  "OPENCV_TRAITS_ENABLE_DEPRECATED"
  "USE_LEVELDB"
  "USE_LMDB"
  "USE_OPENCV"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "/home/ubuntu/Downloads/2-Linux/OpenNI-Linux-Arm-2.3/Include"
  "/usr/local/include"
  "/usr/local/include/opencv"
  "/usr/local/cuda-6.5/include"
  "/usr/include/atlas"
  "/home/ubuntu/caffe-rc5/include"
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
