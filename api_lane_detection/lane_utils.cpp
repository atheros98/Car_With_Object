#include "lane_utils.h"

/*Support functions*/
Mat grayScale(Mat src) {
    Mat dst;
    cvtColor(src, dst, COLOR_BGR2GRAY);
    threshold(dst, dst, thresh_day, 255, THRESH_BINARY);
    return dst;
}

Mat ROI(Mat src, int vect[]) {
    Mat roi;
    roi = src(Rect(vect[0], vect[1], vect[2], vect[3]));
    return roi;
}

Mat colorFilter(Mat src) {

    Mat dst, finalDst;
    cvtColor(src, dst, COLOR_BGR2HLS);
    inRange(dst, Scalar(0, 200, 0), Scalar(150, 255, 255), dst); //2 Scalar is the three channel range
    return dst;
}

//Mat smoothImage(Mat src) {
//    Mat dst = src.clone();
//    int count_black = 0;
//    int count_white = 0;
//    int offset = 4;
//    int threshold = (int) (offset * offset * 4) * 60 / 100;
//    for (int y = offset; y < dst.rows - offset; y++) {
//        for (int x = offset; x < dst.cols - offset; x++) {
//            if (dst.at<uchar>(y, x) == 255) {
//                count_black = 0;
//                for (int a = y - offset; a < y + offset; a++) {
//                    for (int b = x - offset; b < x + offset; b++) {
//                        if (dst.at<uchar>(a, b) == 0) {
//                            count_black++;
//                        }
//                    }
//                }
//                if (count_black > threshold)
//                    dst.at<uchar>(y, x) = 0;
//            } else {
//                count_white = 0;
//                for (int a = y - offset; a < y + offset; a++) {
//                    for (int b = x - offset; b < x + offset; b++) {
//                        if (dst.at<uchar>(a, b) == 255) {
//                            count_white++;
//                        }
//                    }
//                }
//                if (count_white > threshold)
//                    dst.at<uchar>(y, x) = 255;
//            }
//        }
//    }
//    return dst;
//}

Mat birdEyeView(Mat src) {
    Mat dst;
    int alpha_ = 12, beta_ = 90, gamma_ = 90;
    int f_ = 990, dist_ = 230;
    double PI = 3.141592;
    double focalLength, dist, alpha, beta, gamma;

    alpha = ((double) alpha_ - 90) * PI / 180;
    beta = ((double) beta_ - 90) * PI / 180;
    gamma = ((double) gamma_ - 90) * PI / 180;
    focalLength = (double) f_;
    dist = (double) dist_;

    Size image_size = src.size();
    double w = (double) image_size.width, h = (double) image_size.height;


    // Projecion matrix 2D -> 3D
    Mat A1 = (Mat_<float>(4, 3) <<
              1, 0, -w / 2,
              0, 1, -h / 2,
              0, 0, 0,
              0, 0, 1);


    // Rotation matrices Rx, Ry, Rz

    Mat RX = (Mat_<float>(4, 4) <<
              1, 0, 0, 0,
              0, cos(alpha), -sin(alpha), 0,
              0, sin(alpha), cos(alpha), 0,
              0, 0, 0, 1);

    Mat RY = (Mat_<float>(4, 4) <<
              cos(beta), 0, -sin(beta), 0,
              0, 1, 0, 0,
              sin(beta), 0, cos(beta), 0,
              0, 0, 0, 1);

    Mat RZ = (Mat_<float>(4, 4) <<
              cos(gamma), -sin(gamma), 0, 0,
              sin(gamma), cos(gamma), 0, 0,
              0, 0, 1, 0,
              0, 0, 0, 1);


    // R - rotation matrix
    Mat R = RX * RY * RZ;

    // T - translation matrix
    Mat T = (Mat_<float>(4, 4) <<
             1, 0, 0, 0,
             0, 1, 0, 0,
             0, 0, 1, dist,
             0, 0, 0, 1);

    // K - intrinsic matrix
    Mat K = (Mat_<float>(3, 4) <<
             focalLength, 0, w / 2, 0,
             0, focalLength, h / 2, 0,
             0, 0, 1, 0
            );


    Mat transformationMat = K * (T * (R * A1));

    warpPerspective(src, dst, transformationMat, image_size, INTER_CUBIC | WARP_INVERSE_MAP);

    return dst;
}

Vec4f functionGet(Vec4f l) {
    Vec4f func;
    double x0, y0, a, b;
    a = l[0];
    b = l[1];
    x0 = l[2];
    y0 = l[3];

    func[0] = b / a;
    func[1] = y0 - b / a*x0;

    return func;
}

Point intersectOfTwoFunc(Vec4f f, Vec4f s) {
    Vec4f funcF = functionGet(f);
    Vec4f funcS = functionGet(s);
    double a1, b1, a2, b2;
    a1 = funcF[0];
    b1 = funcF[1];
    a2 = funcS[0];
    b2 = funcS[1];

    Point intersect;
    intersect.y = (b1 * a2 - b2 * a1) / (a2 - a1);
    intersect.x = (b1 - intersect.y) / a1;

    return intersect;
}

Mat contoursMat(Mat src) {
    Mat dst;
    vector<vector<Point> > contours;
    vector<vector<Point> > newContours(contours.size());
    vector<Vec4i> hierarchy;

    findContours(src, contours, hierarchy, RETR_TREE, CHAIN_APPROX_SIMPLE, Point(0, 0));
    vector<Point> approxShape;
    dst = Mat::zeros(src.size(), CV_8UC1);
    for (size_t i = 0; i < contours.size(); i++) {
        Scalar color = Scalar(255, 255, 255);
        drawContours(dst, contours, i, 255, CV_FILLED); // fill white
    }

    return dst;
}

vector<vector<Point> > contoursGet(Mat src) {

    Mat dst;
    vector<vector<Point> > contours;
    vector<vector<Point> > newContours;
    vector<Vec4i> hierarchy;

    findContours(src, contours, hierarchy, RETR_TREE, CHAIN_APPROX_SIMPLE, Point(0, 0));
    for (size_t i = 0; i < contours.size(); i++) {
        if (contourArea(contours.at(i)) > 120) {
            newContours.push_back(contours.at(i));
        }
    }
    return newContours;
}
/*Group all the countours as one contour*/
vector<Point> contourFilter(vector<vector<Point> > contours) {
    vector<Point> newContour;
    for (size_t i = 0; i < contours.size(); i++) {
        vector<Point> cnt = contours.at(i);
        for (int j = 0; j < cnt.size(); j++) {
            Point pt = cnt.at(j);
            newContour.push_back(pt);
        }

    }
    return newContour;
}
/*Use fitLine to get the function*/
Vec4f lineGet(vector<Point> contour) {
    Vec4f l;
    fitLine(contour, l, CV_DIST_L2, 0, 0.01, 0.01);
    return l;
}

/*Input is the function of one graph
 return the alpha slope with the horizontal axis*/
double getSlope(Vec4f l) {
    double a, b;
    double pi = 3.1415926;
    a = l[0];
    b = l[1];
    double slope = atan(b / a);
    slope = slope * 180 / pi;
    return -slope;

}

/*Draw line when knowing function of that line*/
Mat drawLine(Mat src, Vec4f l, Point lastPoint) {
    Mat dst;
    if (src.channels() < 3) {
        dst = Mat::zeros(src.rows, src.cols, CV_8UC3);
    } else {
        dst = src.clone();
    }
    double x0, y0, a, b;
    a = l[0];
    b = l[1];
    x0 = l[2];
    y0 = l[3];
    //Check if line data is empty or available
    if (b == 0) {
        return dst;
    }
    if (lastPoint.x <= 0 && lastPoint.y <= 0) {

        //Calculate to return to 2 points' index
        Point first, second;
        first.x = x0;
        first.y = y0;
        second.x = a / b * (-y0) + x0;
        second.y = 0;
        //Draw line
        Scalar color = Scalar(0, 255, 255);
        line(dst, first, second, color, 1, LINE_8);
    }
    return dst;
}
/*Input is one vec4b containing line function: vx,vy,x,y
 Then draws the graph of this function on one Mat*/
Mat drawLANE(Mat src, Vec4f l) {
    Mat dst;
    if (src.channels() < 3) {
        dst = Mat::zeros(src.rows, src.cols, CV_8UC3);
    } else {
        dst = src.clone();
    }
    double x0, y0, a, b;
    a = l[0];
    b = l[1];
    x0 = l[2];
    y0 = l[3];
    //Check if line data is empty or available
    if (a == 0 && b == 0 && x0 == 0 && y0 == 0) {
        return dst;
    }
    //Calculate to return to 2 points' index
    Point first, second;
    first.x = a / b * (src.rows - y0) + x0;
    first.y = src.rows;
    second.x = a / b * (-y0) + x0;
    second.y = 0;
    //Draw line
    Scalar color = Scalar(255, 255, 255);
    line(dst, first, second, color, 1, LINE_8);

    return dst;
}
/*Get a group of contour as input data
 Return the contour of centeroid of these contours*/
vector<Point> momentGet(vector<vector<Point> >contours) {
    /// Get the moments
    vector<Moments> mu(contours.size());
    for (int i = 0; i < contours.size(); i++) {
        mu[i] = moments(contours[i], true);
    }

    ///  Get the mass centers:
    vector<Point> mc(contours.size());
    for (int i = 0; i < contours.size(); i++) {
        mc[i] = Point(mu[i].m10 / mu[i].m00, mu[i].m01 / mu[i].m00);
    }
    return mc;
}
/*Put text on one position on Mat*/
Mat putTextOnMat(Mat src, double number, Point position ) {
    Mat dst;
    dst = Mat::zeros(src.size(), CV_8UC3);

    char txt[30];

    sprintf(txt, "F=%f", number);
    putText(dst, txt, position, FONT_HERSHEY_SIMPLEX, .7, Scalar(0, 0, 255), 2, 8, false);

    return dst;
}

/*Receive two input angle, return the steering angle*/
double getSteeringAngle(double alpha, double beta) {
    double leftAng, rightAng, steeringAng;
    leftAng = reverseAng(alpha);
    rightAng = reverseAng(beta);

    steeringAng = (leftAng + rightAng);

    return steeringAng;
}

/*Cause angle is in the range from -90 to 90
 * So if the angle=alpha is less than -90, the complement with the vertical axis is -90-alpha
 * So if the angle=alpha is less than 90, the complement with the vertical axis is 90-alpha
 * if the angle is too small, nearly 0, return 0.
 */
double reverseAng(double value) {
    double result;
    if (std::isnan(value)) {
        return 0;
    } else if (value > -0.1 && value < 0.1) {
        result = 0;
    } else if (value <= -0.1) {
        result = -90 - value;
    } else if (value >= 0.1) {
        result = 90 - value;
    }
    return result;
}
