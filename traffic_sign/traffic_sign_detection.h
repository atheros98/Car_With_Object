/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   traffic_sign_detection.h
 * Author: atheros
 *
 * Created on March 15, 2018, 6:02 PM
 */

//input: frame
//output: vector of candidate sign

#ifndef TRAFFIC_SIGN_DETECTION_H
#define TRAFFIC_SIGN_DETECTION_H

//include library here
#include <opencv2/opencv.hpp>
using namespace std;
using namespace cv;

//declare function prototype
Mat blue_detector(Mat src);
Mat white_detector(Mat src);
Mat red_detector(Mat src);
Mat ROI(Mat src,int x,int y,int width,int height);
vector<Mat> extract_candidate_img(Mat src,Mat raw,bool isRed);
vector<Mat> api_traffic_sign_detection(Mat src);

vector<Mat> extract_candidate_img_visual(Mat src,Mat& raw,bool isRed);
vector<Mat> api_traffic_sign_detection_visual(Mat& src);

#endif /* TRAFFIC_SIGN_DETECTION_H */

