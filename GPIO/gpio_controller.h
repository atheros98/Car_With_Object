#ifndef GPIO_CONTROLLER_H_INCLUDED
#define GPIO_CONTROLLER_H_INCLUDED

//include library here
#include "Hal.h"
#include <iostream>
using namespace EmbeddedFramework;
using namespace std;
//define constant here
#define SW1_PIN 160
#define SW2_PIN 161
#define SW3_PIN 163
#define SW4_PIN 164
#define SENSOR  165
#define LED     166


extern int sw1_value;
extern int sw2_value;
extern int sw3_value;
extern int sw4_value;
extern int sensor_value;
extern int leb_value;
extern GPIO *gpio;

int init_gpio();
int get_current_value();

#endif // GPIO_CONTROLLER_H_INCLUDED
