#include "gpio_controller.h"

int sw1_value;
int sw2_value;
int sw3_value;
int sw4_value;
int sensor_value;
int leb_value;
GPIO *gpio = new GPIO();

int init_gpio() {
    sw1_value = 1;
    sw2_value = 1;
    sw3_value = 1;
    sw4_value = 1;
    sensor_value = 1;

    gpio->gpioExport(SW1_PIN);
    gpio->gpioExport(SW2_PIN);
    gpio->gpioExport(SW3_PIN);
    gpio->gpioExport(SW4_PIN);
    gpio->gpioExport(SENSOR);
    gpio->gpioExport(LED);

    gpio->gpioSetDirection(SW1_PIN, INPUT);
    gpio->gpioSetDirection(SW2_PIN, INPUT);
    gpio->gpioSetDirection(SW3_PIN, INPUT);
    gpio->gpioSetDirection(SW4_PIN, INPUT);
    gpio->gpioSetDirection(SENSOR, INPUT);
    gpio->gpioSetDirection(LED, OUTPUT);

    usleep(10000);
}

int get_current_value() {
    unsigned int bt_status;

    gpio->gpioGetValue(SW1_PIN, &bt_status);
    if (bt_status != sw1_value) {
        return 1;
    }

    gpio->gpioGetValue(SW2_PIN, &bt_status);
    if (!bt_status) {
        if (bt_status != sw2_value) {
            sw2_value = bt_status;
            return 2;
        }
    } else sw2_value = bt_status;


    gpio->gpioGetValue(SW3_PIN, &bt_status);
    if (bt_status != sw3_value) {
        return 3;
    }

    gpio->gpioGetValue(SW4_PIN, &bt_status);
    if (bt_status != sw4_value) {
        return 4;
    }
    return 0;
}
