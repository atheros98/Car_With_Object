#include "video_logger.h"

VideoWriter color_writer;
VideoWriter gray_writer;
int codec = CV_FOURCC('H','2','6', '4');

void init_video_writer(Size gray_video_size) {
    color_writer.open(color_name, codec, 8,  Size(320,240), true);
    gray_writer.open(gray_name, codec, 8,  gray_video_size, true);

}
void log_color(Mat color){
    color_writer.write(color);
}
void log_gray(Mat gray){
    gray_writer.write(gray);
}
void release_video_writer() {
    color_writer.release();
    gray_writer.release();
}
