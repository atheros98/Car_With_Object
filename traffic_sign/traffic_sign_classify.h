/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   traffic_sign_classifiy.h
 * Author: atheros
 *
 * Created on March 18, 2018, 1:34 PM
 */

#ifndef TRAFFIC_SIGN_CLASSIFIY_H
#define TRAFFIC_SIGN_CLASSIFIY_H

//include library here

#include <caffe/caffe.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/opencv.hpp>
#include <algorithm>
#include <iosfwd>
#include <memory>
#include <string>
#include <utility>
#include <vector>
#include <chrono>
#include <iostream>

using namespace caffe; // NOLINT(build/namespaces)
using std::string;


//defince util function here

/* Pair (label, confidence) representing a prediction. */
typedef std::pair<string, float> Prediction;

class Classifier {
public:
    Classifier(const string& model_file,
               const string& trained_file,
               const string& mean_file,
               const string& label_file);

    std::vector<Prediction> Classify(const cv::Mat& img, int N = 5);

private:
    void SetMean(const string& mean_file);

    std::vector<float> Predict(const cv::Mat& img);

    void WrapInputLayer(std::vector<cv::Mat>* input_channels);

    void Preprocess(const cv::Mat& img,
                    std::vector<cv::Mat>* input_channels);

private:
    boost::shared_ptr<Net<float> > net_;
    cv::Size input_geometry_;
    int num_channels_;
    cv::Mat mean_;
    std::vector<string> labels_;
};

//declare variable here

extern string model_file;
extern string trained_file;
extern string mean_file;
extern string label_file;
extern Classifier classifier;

Prediction getLabel(cv::Mat img);
int evaluate_frame(cv::Mat frame);

#endif /* TRAFFIC_SIGN_CLASSIFIY_H */

