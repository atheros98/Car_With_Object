#include "camera_controller.h"

VideoStream color_stream;
VideoStream depth_stream;

Device device;

void init_device() {
    Status rc;

    rc = OpenNI::initialize();
    if (rc != STATUS_OK) {
        printf("Initialize failed\n%s\n", OpenNI::getExtendedError());
        return;
    }
    rc = device.open(ANY_DEVICE);
    if (rc != STATUS_OK) {
        printf("Couldn't open device\n%s\n", OpenNI::getExtendedError());
        return;
    }

}


void init_color_stream() {
    Status rc;

    if (device.getSensorInfo(SENSOR_COLOR) != NULL) {
        rc = color_stream.create(device, SENSOR_COLOR);
        if (rc == STATUS_OK) {
            VideoMode color_mode = color_stream.getVideoMode();
            color_mode.setFps(30);
            color_mode.setResolution(VIDEO_FRAME_WIDTH, VIDEO_FRAME_HEIGHT);
            color_mode.setPixelFormat(PIXEL_FORMAT_RGB888);
            color_stream.setVideoMode(color_mode);
            rc = color_stream.start();
            if (rc != STATUS_OK) {
                printf("Couldn't start the color stream\n%s\n", OpenNI::getExtendedError());
            }
        } else {
            printf("Couldn't create color stream\n%s\n", OpenNI::getExtendedError());
        }
    }
}

void init_depth_stream() {
    Status rc;

    if (device.getSensorInfo(SENSOR_DEPTH) != NULL) {
        rc = depth_stream.create(device, SENSOR_DEPTH);
        if (rc == STATUS_OK) {
            VideoMode depth_mode = depth_stream.getVideoMode();
            depth_mode.setFps(30);
            depth_mode.setResolution(VIDEO_FRAME_WIDTH, VIDEO_FRAME_HEIGHT);
            depth_mode.setPixelFormat(PIXEL_FORMAT_DEPTH_100_UM);
            depth_stream.setVideoMode(depth_mode);

            rc = depth_stream.start();
            if (rc != STATUS_OK) {
                printf("Couldn't start the color stream\n%s\n", OpenNI::getExtendedError());
            }
        } else {
            printf("Couldn't create depth stream\n%s\n", OpenNI::getExtendedError());
        }
    }


}

Mat get_color_frame() {
    VideoFrameRef color_frame;
    RGB888Pixel* color_data;

    color_stream.readFrame(&color_frame);

    Mat image = Mat(VIDEO_FRAME_HEIGHT,VIDEO_FRAME_WIDTH, CV_8UC3);
    color_data = (RGB888Pixel*)color_frame.getData();

    memcpy(image.data, color_data, VIDEO_FRAME_WIDTH*VIDEO_FRAME_HEIGHT*sizeof(RGB888Pixel));
    cvtColor(image, image, COLOR_RGB2BGR);
    flip(image, image, 1);

    return image;
}

Mat get_depth_frame() {
    VideoFrameRef depth_frame;
    DepthPixel* depth_data;

    depth_stream.readFrame(&depth_frame);

    Mat image = Mat(VIDEO_FRAME_HEIGHT, VIDEO_FRAME_WIDTH, CV_16U);

    depth_data = (DepthPixel*)depth_frame.getData();
    memcpy(image.data, depth_data, VIDEO_FRAME_WIDTH*VIDEO_FRAME_HEIGHT*sizeof(DepthPixel));
    normalize(image, image, 255, 0, NORM_MINMAX);
    image.convertTo(image, CV_8U);

    return image;
}


void camera_release() {
    if (color_stream.isValid()) {
        color_stream.destroy();
    }
    if (depth_stream.isValid()) {
        depth_stream.destroy();
    }

    device.close();
    OpenNI::shutdown();
}
