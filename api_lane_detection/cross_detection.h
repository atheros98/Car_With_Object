#ifndef CROSS_DETECTION_H
#define CROSS_DETECTION_H
#include "opencv2/imgcodecs.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/highgui.hpp"
#include <iostream>
#include <fstream>
#include "lane_utils.h"
using namespace std;
using namespace cv;

/*Global variables*/
//defince constant


//declare variable here
extern double upper_thresh_slope;
extern double lower_thresh_slope;

/*----------------*/

bool cross_check(Mat src);
bool cross_check_visual(Mat src);
#endif /* CROSS_DETECTION_H */
