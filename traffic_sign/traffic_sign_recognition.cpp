#include "traffic_sign_recognition.h"

int counter = 0;
string pre_label = "";

int evaluate_frame(Mat frame) {
    int int_label = -1;
    vector<Mat> detected_imgs = api_traffic_sign_detection(frame);
    //get max label
    double max_confidence = 0;
    Prediction p_max;
    for (int i = 0; i < detected_imgs.size(); i++) {
        Prediction p = getLabel(detected_imgs[i]);
        if (p.second > max_confidence) {
            p_max = p;
            max_confidence = p_max.second;
        }
    }

    //increase counter if pre_label = current label (p_max.first)
    if (p_max.first == pre_label) {
        counter++;
    } else {
        counter = 0;
    }

    pre_label = p_max.first;
    //cout <<counter <<"--------"<<endl;
    if (counter > stack_times) {
        if (p_max.first == "0 turn_left_ahead" && p_max.second >= std_cnfdnce) {
            int_label = 0;
        } else if (p_max.first == "1 turn_right_ahead" && p_max.second >= std_cnfdnce) {
            int_label = 1;
        } else if (p_max.first == "2 stop" && p_max.second >= std_cnfdnce) {
            int_label = 2;
        }

        counter = 0;
        pre_label = "";
        cout << p_max.first << endl;
        return int_label;
    }

    return -1;
}

int evaluate_frame_visual(Mat& frame) {
    int int_label = -1;
    vector<Mat> detected_imgs = api_traffic_sign_detection_visual(frame);
    //get max label
    double max_confidence = 0;
    Prediction p_max;
    for (int i = 0; i < detected_imgs.size(); i++) {
        Prediction p = getLabel(detected_imgs[i]);
        if (p.second > max_confidence) {
            p_max = p;
            max_confidence = p_max.second;
        }
    }

    //increase counter if pre_label = current label (p_max.first)
    if (p_max.first == pre_label) {
        counter++;
    } else {
        counter = 0;
    }

    pre_label = p_max.first;
    //cout <<counter <<"--------"<<endl;
    if (counter > stack_times) {
        if (p_max.first == "0 turn_left_ahead" && p_max.second >= std_cnfdnce) {
            int_label = 0;
                    cout << p_max.first << endl;

        } else if (p_max.first == "1 turn_right_ahead" && p_max.second >= std_cnfdnce) {
            int_label = 1;
                    cout << p_max.first << endl;

        } else if (p_max.first == "2 stop" && p_max.second >= std_cnfdnce) {
            int_label = 2;
        }

        counter = 0;
        pre_label = "";
        putText(frame, p_max.first, Point(80, 20), FONT_HERSHEY_SIMPLEX, .7, Scalar(0, 0, 255), 2, 8, false);

    }
    imshow("Traffic Sign", frame);
    return int_label;
}

