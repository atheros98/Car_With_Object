/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   traffic_sign_recognition.h
 * Author: atheros
 *
 * Created on March 18, 2018, 2:17 PM
 */

#ifndef TRAFFIC_SIGN_RECOGNITION_H
#define TRAFFIC_SIGN_RECOGNITION_H

#include "traffic_sign_detection.h"
#include "traffic_sign_classify.h"

//define const here
const int stack_times = 0;
const double std_cnfdnce = 0.9;


//declare variable here
extern int counter;
extern string pre_label;


int evaluate_frame(Mat frame);
int evaluate_frame_visual(Mat& frame);



#endif /* TRAFFIC_SIGN_RECOGNITION_H */

