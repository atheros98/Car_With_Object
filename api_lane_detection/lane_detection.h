#ifndef LANE_DETECTION_H
#define LANE_DETECTION_H

#include "opencv2/imgcodecs.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/highgui.hpp"
#include <iostream>
#include <fstream>
#include "lane_utils.h"
using namespace std;
using namespace cv;

/*Global variables*/
extern double steering_angle;
extern Point center;
extern bool startedSet;
extern int halfRoadWidth;
extern Point preCenter;
/*----------------*/
/*Using angle*/
double lane_steering(Mat src);
double lane_steering_visual(Mat src, Mat &refFrame);

/*Using center*/
double lane_detection_center(Mat src);
double lane_detection_center_visual(Mat src, Mat &refFrame);
double getSlopeThroughTwoPoints(Point first, Point second);
double getTheta(Point car, Point dst);
double lane_get_contour(Mat src);
double lane_get_contour_visual(Mat src, Mat &refFrame);
#endif /* LANE_DETECTION_H */

