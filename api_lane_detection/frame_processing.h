#ifndef API_LANE_CONTROLLER_H
#define API_LANE_CONTROLLER_H
#include "opencv2/imgcodecs.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/highgui.hpp"
#include <iostream>
#include <fstream>
#include "lane_utils.h"
using namespace std;
using namespace cv;

/*Global variables*/
/*----------------*/
/*Main function*/
Mat frame_processing(Mat src);
Mat smoothImage(Mat src);
#endif /* API_LANE_CONTROLLER_H */
