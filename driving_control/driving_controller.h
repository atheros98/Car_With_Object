#ifndef DRIVING_CONTROLLER_H_INCLUDED
#define DRIVING_CONTROLLER_H_INCLUDED

//include library herer
#include "peripheral_driver/api_i2c_pwm.h"
#include <opencv2/opencv.hpp>
using namespace cv;
using namespace std;


//define constant here


//declare variable here
extern PCA9685 *pca9685;
extern int throttle_value;
extern double steer_angle;
extern bool running;
extern bool stopped;
extern bool printed;
//declare function prototype here
int init_engine(int init_throttle);
int control_car(double angle, bool& has_cross, int& sign_label, int bt_status);
int control_car(double angle, bool has_cross, int bt_status);

void car_run();
void car_pause();
void car_stop();


#endif // DRIVING_CONTROLLER_H_INCLUDED
