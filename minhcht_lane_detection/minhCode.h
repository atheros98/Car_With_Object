#include "opencv2/imgcodecs.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/highgui.hpp"
#include <iostream>
#include <fstream>
#include <set>
#include <algorithm>
#include <stdlib.h>
#include <bits/c++config.h>

using namespace std;
using namespace cv;

extern int VECT[4];
extern int NEEDEDFRAME;
extern int BEGINF;
extern int CONS ;
extern int SENST ;
extern bool NEEDWRITE ;
extern int  CROSSSENT ;
extern int preState;
extern int LANESENT;

Mat birdViewTransform(Mat src);
Mat MROI(Mat src, int vect[]);
Mat drawLine(Mat src,Mat raw);
Mat drawCenter(Mat src);
vector<Mat> pipeLine(Mat img);
char checkCross(Mat input);
char isCross(Mat input);
float angleCalculator(char stage,char sign,Mat drawed,Mat centered);
bool isTwoLane(Mat drawed);
vector<vector<Point> > getExPoint(Mat input);
float angleByAtan2(Point first,Point second);
float straightCal(Mat input);
float turnCal(Mat input);
float crossCal(char sign,Mat input);
float angleProcess(Mat centered,Mat drawed);
bool crossProcess(Mat drawed);
bool crossProcessVisual(Mat& check,Mat drawed);
float angleProcessVisual(Mat& check,Mat centered,Mat drawed);
int keepCenter(Mat drawed);
